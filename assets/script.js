const menuButton = document.querySelector("#mainMenuButton");
const mainNav = document.querySelector(".mainNav-nav-list");

menuButton.addEventListener("click", function () {
    if (mainNav.style.display === 'none' || mainNav.style.display === '') {
        mainNav.style.display = 'flex';
    }
    else {
        mainNav.style.display = 'none';
    }
});
