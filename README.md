# Portfolio

Personal portfolio website

## Set-up

```sh
pdm install
```

## Run

```sh
pdm serve
```

## Build for production

```sh
pdm prod
```
